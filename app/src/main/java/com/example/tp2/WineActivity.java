package com.example.applicationmobiletp2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends Activity {
    Wine wine = new Wine();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        final WineDbHelper wineDb = new WineDbHelper(this);
        final Bundle extras = getIntent().getExtras();
        if(extras != null) {
            wine = getIntent().getParcelableExtra("wine");
            String title = wine.getTitle();
            String region = wine.getRegion();
            String localization = wine.getLocalization();
            String climate = wine.getClimate();
            String plantedArea = wine.getPlantedArea();
            ((EditText) findViewById(R.id.wineName)).setText(title);
            ((EditText) findViewById(R.id.editWineRegion)).setText(region);
            ((EditText) findViewById(R.id.editLoc)).setText(localization);
            ((EditText) findViewById(R.id.editClimate)).setText(climate);
            ((EditText) findViewById(R.id.editPlantedArea)).setText(plantedArea);
        }

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = ((EditText) findViewById(R.id.wineName)).getText().toString();
                String region = ((EditText) findViewById(R.id.editWineRegion)).getText().toString();
                String localization = ((EditText) findViewById(R.id.editLoc)).getText().toString();
                String climate = ((EditText) findViewById(R.id.editClimate)).getText().toString();
                String plantedArea = ((EditText) findViewById(R.id.editPlantedArea)).getText().toString();

                wine.setTitle(title);
                wine.setRegion(region);
                wine.setLocalization(localization);
                wine.setClimate(climate);
                wine.setPlantedArea(plantedArea);
                if(extras != null) {
                    wineDb.updateWine(wine);
                }
                else {
                    wineDb.addWine(wine);
                }

                Toast.makeText(WineActivity.this, "Vin sauvegardé", Toast.LENGTH_LONG).show();
                finish();
                Intent intent = new Intent(WineActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });
    }
}
