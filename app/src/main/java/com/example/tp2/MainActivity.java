package com.example.applicationmobiletp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String WINE = "com.example.tp2.WINE";
    public static final String ID = "com.example.tp2.id";
    private static final String TAG = MainActivity.class.getSimpleName();
    private ListView wineList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final WineDbHelper wineDb = new WineDbHelper(this);
        wineDb.populate();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_2,wineDb.fetchAllWines(),
                new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[] {android.R.id.text1, android.R.id.text2},0);
        wineList = findViewById(R.id.listWine);
        wineList.setAdapter(adapter);
        registerForContextMenu(wineList);
        wineList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SQLiteDatabase db = wineDb.getReadableDatabase();
                Cursor cursor = null;
                String cId = "_id=" + Long.toString(id);
                cursor = db.query(WineDbHelper.TABLE_NAME, null,cId,null,null,null,null);
                Wine wine = null;
                if(cursor != null) {
                    wine = WineDbHelper.cursorToWine(cursor);
                }
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine",wine);
                startActivity(intent);
            }
        });
        FloatingActionButton button = findViewById(R.id.fab);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                startActivity(intent);
            }
        });
    }

    public void OnCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu,v,menuInfo);
        menu.add("Supprimer");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if(item.getTitle() == "Supprimer") {
            AdapterView wineList = findViewById(R.id.listWine);
            Cursor cursor = (Cursor) wineList.getItemAtPosition(info.position);
            final WineDbHelper wineDb = new WineDbHelper(this);
            if(cursor != null) {
                wineDb.deleteWine(cursor);
            }
            finish();
            startActivity(getIntent());
            Toast.makeText(getApplicationContext(), "Vin supprimé", Toast.LENGTH_SHORT).show();
        }
        return super.onContextItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
