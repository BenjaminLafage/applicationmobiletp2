package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE   " + TABLE_NAME + "(" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME + " TEXT NOT NULL," +
                COLUMN_WINE_REGION + " TEXT NOT NULL," +
                COLUMN_LOC + " TEXT NOT NULL," +
                COLUMN_CLIMATE + " TEXT NOT NULL," +
                COLUMN_PLANTED_AREA + " TEXT NOT NULL );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        String title = wine.getTitle();
        String region = wine.getRegion();
        String localization = wine.getLocalization();
        String climate = wine.getClimate();
        String plantedArea = wine.getPlantedArea();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, title);
        values.put(COLUMN_WINE_REGION,region);
        values.put(COLUMN_LOC, localization);
        values.put(COLUMN_CLIMATE, climate);
        values.put(COLUMN_PLANTED_AREA, plantedArea);
        // Inserting Row
        long rowID = 0;
        rowID = db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
	    int res;
        ContentValues cvalues = new ContentValues();
        cvalues.put("_id", wine.getId());
        cvalues.put("title", wine.getTitle());
        cvalues.put("region", wine.getRegion());
        cvalues.put("localization", wine.getLocalization());
        cvalues.put("climate", wine.getClimate());
        cvalues.put("plantedArea", wine.getPlantedArea());
        res = db.update(TABLE_NAME, cvalues, "_id=" + wine.getId(), null);
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

	    Cursor cursor = db.query(this.TABLE_NAME, new String[] {"ROWID AS __id", COLUMN_NAME,COLUMN_WINE_REGION,COLUMN_LOC,COLUMN_CLIMATE,COLUMN_PLANTED_AREA},null,null,null,null,COLUMN_NAME);
	// call db.query()
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, _ID + "=?", new String[]{cursor.getString(cursor.getColumnIndex(_ID))});
        db.close();
    }

     public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        cursor.moveToFirst();
        wine = new Wine();
        Long id = cursor.getLong(cursor.getColumnIndex("_id"));
        String title = cursor.getString(cursor.getColumnIndex("title"));
        String region = cursor.getString(cursor.getColumnIndex("region"));
        String localization = cursor.getString(cursor.getColumnIndex("localization"));
        String climate = cursor.getString(cursor.getColumnIndex("climate"));
        String plantedArea = cursor.getString(cursor.getColumnIndex("PlantedArea"));
        wine = new Wine(id, title, region, localization, climate, plantedArea);
        return wine;
    }
}
